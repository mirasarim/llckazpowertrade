//
//  DataEnteredDelegate.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/18/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import Foundation
import UIKit

// protocol used for sending data back
protocol DataEnteredDelegate: class {
    func userDidEnterInformation(info: UIImage, layer: String)
    func addMaterialToArray(material: String, layer: String)
    func addFeaturesToArray(features: [String])
    func updateVoltage(voltage: String)
}
