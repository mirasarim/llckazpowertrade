//
//  ConductorViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/18/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit
import CoreActionSheetPicker

class ConductorViewController: UIViewController {
    
    var selectedMaterial = #imageLiteral(resourceName: "med")
    var selectedMaterialString = ""
    var layer = ""
    var voltage = ""
    var conductorsAmount = ""
    weak var delegate: DataEnteredDelegate?
    weak var pagingDelegate: PagingMenuDelegate?
    @IBOutlet weak var conductorsAmountTextField: UITextField!
    @IBOutlet weak var voltageTextField: UITextField!
    @IBOutlet weak var selectMaterialButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.voltageTextField.text = voltage
        self.conductorsAmountTextField.text = conductorsAmount
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        self.view.endEditing(true)
        pagingDelegate?.moveTo(pageIndex: 1)
    }

    @IBAction func selectMaterialButtonPressed(_ sender: Any) {
        self.view.endEditing(true)
        showACP(sender)
    }

    func showACP(_ sender: Any) {
        let bonusesToACP = ["Медь", "Алюминий"]
        
        if let acp = ActionSheetStringPicker(title: "Материалы:", rows: bonusesToACP, initialSelection: 0, doneBlock: {
            picker, indexes, values in
//            print("\n--- \(indexes) \n--- \(values)")
            self.selectedMaterialString = values as! String
            if values as! String == "Алюминий" {
                self.selectedMaterial = #imageLiteral(resourceName: "aluminium")
                self.selectMaterialButton.setTitle("Выберите материал. (Алюминий)", for: .normal)
            } else {
                self.selectedMaterial = #imageLiteral(resourceName: "med")
                self.selectMaterialButton.setTitle("Выберите материал. (Медь)", for: .normal)
            }
            
            return
        }, cancel: { ActionSheetStringPicker in return }, origin: sender) {
            acp.setDoneButton(UIBarButtonItem(title: "Готово", style: .plain, target: nil, action: nil))
            acp.setCancelButton(UIBarButtonItem(title: "Отмена", style: .plain, target: nil, action: nil))
            acp.show()
        }
    }

    func updateFromDelegate() {
        delegate?.userDidEnterInformation(info: selectedMaterial, layer: self.layer)
        delegate?.addMaterialToArray(material: selectedMaterialString, layer: self.layer)
        delegate?.updateVoltage(voltage: voltageTextField.text!)
    }
}

