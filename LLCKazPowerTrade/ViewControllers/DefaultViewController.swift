//
//  DefaultViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/19/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit
import CoreActionSheetPicker

class DefaultViewController: UIViewController {

    @IBOutlet weak var selectMaterialButton: UIButton!
    weak var delegate: DataEnteredDelegate?
    weak var pagingDelegate: PagingMenuDelegate?
    var materialsDictionary:[String:UIImage] = [:]
    var selectedMaterial:UIImage?
    var selectedMaterialString = ""
    var layer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func selectMaterialButtonPressed(_ sender: Any) {
        showACP(sender)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        print("default")
        if let uwSelectedMaterial = selectedMaterial{
            delegate?.userDidEnterInformation(info: uwSelectedMaterial, layer: layer)
            delegate?.addMaterialToArray(material: selectedMaterialString, layer: layer)
            
            if layer == "externalLayer" {
                let _ = self.navigationController?.popToRootViewController(animated: true)
            } else if layer == "shieth" {
                pagingDelegate?.moveTo(pageIndex: 4)
            } else if layer == "ekran" {
                pagingDelegate?.moveTo(pageIndex: 3)
            }
        }
    }

    
    func showACP(_ sender: Any) {
        let bonusesToACP = Array(materialsDictionary.keys)
        
        if let acp = ActionSheetStringPicker(title: "Материалы:", rows: bonusesToACP, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            self.selectedMaterialString = values as! String
//            print("\(picker) \n--- \(indexes) \n--- \(values)")
            for (material, texture) in self.materialsDictionary {
                if material == values as! String {
                    self.selectMaterialButton.setTitle("Выберите материал. (\(material))", for: .normal)
                    self.selectedMaterial = texture
                }
            }

            return
        }, cancel: { ActionSheetStringPicker in return }, origin: sender) {
            acp.setDoneButton(UIBarButtonItem(title: "Готово", style: .plain, target: nil, action: nil))
            acp.setCancelButton(UIBarButtonItem(title: "Отмена", style: .plain, target: nil, action: nil))
            acp.show()
        }
    }
}
