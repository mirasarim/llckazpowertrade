//
//  EditCableViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/24/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit
import PagingMenuController

class EditCableViewController: UIViewController {
    
    var rootViewController: UIViewController?
    var optionalPagingMenuController: PagingMenuController?
    var cableDictionary:[String:UIImage] = [:]
    var materials:[String:String] = [:]
    var features:[String] = []
    var voltage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let uwRootViewController = self.navigationController!.viewControllers.first as? GameViewController else {
            print("no root view controller, kappa")
            return
        }
        
        rootViewController = uwRootViewController
        
        self.initializePagingMenuController()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        let barButton = UIBarButtonItem()
        barButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = barButton
    }
    
    func initializePagingMenuController() {
        
        let options = PagingMenuOptions(_rootVC: self.rootViewController!, _selfVC: self)
        optionalPagingMenuController = PagingMenuController(options: options)
        
        guard let pagingMenuController = optionalPagingMenuController else {
            print("no paging meny controller")
            return
        }
        
        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParentViewController: self)
        
        pagingMenuController.view.frame.origin.y += 64
        pagingMenuController.view.frame.size.height -= 64
        
        pagingMenuController.onMove = { state in
            switch state {
            case let .willMoveController(menuController, previousMenuController):
                if let conductorVC = previousMenuController as? ConductorViewController {
                    conductorVC.updateFromDelegate()
                }
                print(previousMenuController)
                print(menuController)
                break
            default:
                break
            }
        }
    }
    
    private struct PagingMenuOptions: PagingMenuControllerCustomizable {
        var rootVC: UIViewController?
        var selfVC: EditCableViewController?
        init(_rootVC: UIViewController, _selfVC: EditCableViewController) {
            self.rootVC = _rootVC
            self.selfVC = _selfVC
        }
        
        fileprivate var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
        }
        
        fileprivate var pagingControllers: [UIViewController] {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if let conductorVC = storyboard.instantiateViewController(withIdentifier: "ConductorViewController") as? ConductorViewController,
                let isolationVC = storyboard.instantiateViewController(withIdentifier: "IsolationViewController") as? IsolationViewController,
                let ekranVC = storyboard.instantiateViewController(withIdentifier: "DefaultViewController") as? DefaultViewController,
                let shiethVC = storyboard.instantiateViewController(withIdentifier: "DefaultViewController") as? DefaultViewController,
                let externalLayerVC = storyboard.instantiateViewController(withIdentifier: "DefaultViewController") as? DefaultViewController {
            
                conductorVC.delegate = rootVC as? DataEnteredDelegate
                conductorVC.pagingDelegate = selfVC
                isolationVC.delegate = rootVC as? DataEnteredDelegate
                isolationVC.pagingDelegate = selfVC
                shiethVC.delegate = rootVC as? DataEnteredDelegate
                shiethVC.pagingDelegate = selfVC
                ekranVC.delegate = rootVC as? DataEnteredDelegate
                ekranVC.pagingDelegate = selfVC
                externalLayerVC.delegate = rootVC as? DataEnteredDelegate
                externalLayerVC.pagingDelegate = selfVC
                
                ekranVC.materialsDictionary
                    = ["Медь проволочная": #imageLiteral(resourceName: "medProvolka"),
                       "Медь ленточная": #imageLiteral(resourceName: "med")]
                shiethVC.materialsDictionary
                    = ["Оцинкованная сталь ленточная": #imageLiteral(resourceName: "ocinkList"),
                       "Оцинкованная сталь проволочная": #imageLiteral(resourceName: "ocinkProvolka")]
                externalLayerVC.materialsDictionary
                    = ["ПВХ": #imageLiteral(resourceName: "pvc"),
                       "Полиэтилен высокой плотности" : #imageLiteral(resourceName: "pvc"),
                       "HFPO" : #imageLiteral(resourceName: "pvc")]
                
                conductorVC.layer = "conductor"
                conductorVC.voltage = selfVC!.voltage
//                conductorVC.conductorsAmountTextField.text = selfVC?.
                if let uwCSM = selfVC?.cableDictionary[conductorVC.layer] {
                    conductorVC.selectedMaterial = uwCSM
                }
                
                isolationVC.layer = "isolation"
                isolationVC.selectedFeatures = selfVC!.features
                if let uwISM = selfVC?.cableDictionary[isolationVC.layer] {
                    isolationVC.selectedMaterial = uwISM
                }
                
                ekranVC.layer = "ekran"
                if let uwESM = selfVC?.cableDictionary[ekranVC.layer] {
                    ekranVC.selectedMaterial = uwESM
                }
                
                shiethVC.layer = "shieth"
                if let uwSSM = selfVC?.cableDictionary[shiethVC.layer] {
                    shiethVC.selectedMaterial = uwSSM
                }
                
                externalLayerVC.layer = "externalLayer"
                if let ELSM = selfVC?.cableDictionary[externalLayerVC.layer] {
                    externalLayerVC.selectedMaterial = ELSM
                }
                
                return [conductorVC,
                        isolationVC,
                        ekranVC,
                        shiethVC,
                        externalLayerVC]
            }
            return [UIViewController()]
        }
        
        fileprivate struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .standard(widthMode: .flexible, centerItem: false, scrollingMode: .pagingEnabled)
            }
            var focusMode: MenuFocusMode {
                return .underline(height: 2, color: UIColor.init(red: 119/255, green: 182/255, blue: 234/255, alpha: 1.0), horizontalPadding: 0, verticalPadding: 0)
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItem1(),
                        MenuItem2(),
                        MenuItem3(),
                        MenuItem4(),
                        MenuItem5()]
            }
        }
        
        fileprivate struct MenuItem1: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText.init(text: "Жило",
                                                      color: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 0.5),
                                                      selectedColor: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0),
                                                      font: .systemFont(ofSize: 17),
                                                      selectedFont: .systemFont(ofSize: 17)))
            }
        }
        
        fileprivate struct MenuItem2: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText.init(text: "Изоляция",
                                                      color: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 0.5),
                                                      selectedColor: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0),
                                                      font: .systemFont(ofSize: 17),
                                                      selectedFont: .systemFont(ofSize: 17)))            }
        }
        
        fileprivate struct MenuItem3: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText.init(text: "Экран",
                                                      color: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 0.5),
                                                      selectedColor: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0),
                                                      font: .systemFont(ofSize: 17),
                                                      selectedFont: .systemFont(ofSize: 17)))
            }
        }
        
        fileprivate struct MenuItem4: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText.init(text: "Броня",
                                                      color: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 0.5),
                                                      selectedColor: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0),
                                                      font: .systemFont(ofSize: 17),
                                                      selectedFont: .systemFont(ofSize: 17)))
            }
        }
        
        fileprivate struct MenuItem5: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText.init(text: "Внешний слой",
                                                      color: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 0.5),
                                                      selectedColor: UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0),
                                                      font: .systemFont(ofSize: 17),
                                                      selectedFont: .systemFont(ofSize: 17)))
            }
        }
    }
}

extension EditCableViewController: PagingMenuDelegate {
    func moveTo(pageIndex: Int) {
        self.optionalPagingMenuController!.move(toPage: pageIndex)
    }
}
