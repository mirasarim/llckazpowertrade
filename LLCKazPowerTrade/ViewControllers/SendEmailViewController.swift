//
//  SendEmailViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/26/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit

class SendEmailViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var requestLabel: UILabel!
    
    var cableDictionary:[String:UIImage] = [:]
    var materials:[String:String] = [:]
    var features:[String] = []
    var voltage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestLabel.text = "Материалы:\n"
        for (layer, material) in materials {
            switch layer {
            case "ekran":
                requestLabel.text = requestLabel.text! + "Экран: \(material)\n"
                break
            case "conductor":
                requestLabel.text = requestLabel.text! + "Жило: \(material)\n"
                break
            case "shieth":
                requestLabel.text = requestLabel.text! + "Броня: \(material)\n"
                break
            case "isolation":
                requestLabel.text = requestLabel.text! + "Изоляция: \(material)\n"
                break
            case "externalLayer":
                requestLabel.text = requestLabel.text! + "Внешний слой: \(material)\n"
                break
            default:
                break
            }
        }
        requestLabel.text = requestLabel.text! + "Доп. возможности:\n"
        for feature in features {
            requestLabel.text = requestLabel.text! + "\(feature)\n"
        }
        requestLabel.text = requestLabel.text! + "Вольтаж: \n\(voltage)кВ."
        
        
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        let barButton = UIBarButtonItem()
        barButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = barButton
    }

    @IBAction func sendEmailButtonPressed(_ sender: Any) {
        print("email sent")
        

    }
}
