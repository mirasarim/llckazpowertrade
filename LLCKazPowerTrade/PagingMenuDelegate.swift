//
//  PagingMenuDelegate.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/25/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import Foundation

import UIKit

// protocol used for sending data back
protocol PagingMenuDelegate: class {
    func moveTo(pageIndex: Int)

}
