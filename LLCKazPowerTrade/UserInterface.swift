//
//  UserInterface.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/18/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit

class UserInterface {
    static let shared = UserInterface()
    
    func showWarningWith(message: String, on: UIViewController){
        let alertController = UIAlertController(title: "Внимание!", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        on.present(alertController, animated: true, completion: nil)
    }
}
