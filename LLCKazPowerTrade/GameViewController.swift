//
//  GameViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/18/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {
    
    @IBOutlet weak var cableName: UILabel!
    @IBOutlet weak var gameView: SCNView!
    @IBOutlet weak var addLayerButton: UIButton!
    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var buttonToBotConstraint: NSLayoutConstraint!
    @IBOutlet weak var twoButtonsConstraint: NSLayoutConstraint!
    var gameScene:SCNScene!
    var cameraNode:SCNNode!
    
    //other variables
    var materials:[String:String] = [:]
    var features:[String] = []
    var cableDictionary:[String:UIImage] = [:]
    var voltage = ""
    var currentHeight:CGFloat = 8.0
    var currentRadius:CGFloat = 0.3
    var currentStep = 0
    
    // view variables
    var globalBackgroundView:UIView?
    var mainView:UIView?
    var aboutUsView:UIView?
    var instructionView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initScene()
        initCamera()
        showMenu(button: UIButton())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        sendEmailButton.setTitle("Отправить заказ", for: .normal)
        var cableNameToShow = ""
        
        if materials.isEmpty {
            buttonToBotConstraint.priority = 500
            addLayerButton.setTitle("Собрать кабель", for: .normal)
            twoButtonsConstraint.priority = 250
        } else {
            buttonToBotConstraint.priority = 900
            addLayerButton.setTitle("Изменить кабель", for: .normal)
            twoButtonsConstraint.priority = 752
        }
        
        for detail in materials.values {
            cableNameToShow.append(fromMaterialToShort(material: detail))
        }
        self.cableName.text = "Наименование:\n\(cableNameToShow)"
        if !self.features.isEmpty {
            var featuresString = ""
            for feature in features {
                featuresString.append("\n\(feature)")
            }
            self.cableName.text = "\(self.cableName.text!)\nСпецификаций:\(featuresString)"
        }
        if !self.voltage.isEmpty {
            self.cableName.text = "\(self.cableName.text!)\nВольтаж: \(self.voltage) кВ"
        }
        
        self.fromDictToCable()
        
    }
    
    func fromDictToCable(){
        // cleaning all nodes from scene
        gameScene.rootNode.enumerateChildNodes { (node, stop) -> Void in
            node.removeFromParentNode()
        }
        initCamera()
        

        if let uwConductorLayer = cableDictionary["conductor"] {
            self.createTargetWith(texture: uwConductorLayer)
        }
        
        if let uwIsolationLayer = cableDictionary["isolation"] {
            self.createTargetWith(texture: uwIsolationLayer)
        }
        
        if let uwEkranLayer = cableDictionary["ekran"] {
            self.createTargetWith(texture: uwEkranLayer)
        }
        
        if let uwShiethLayer = cableDictionary["shieth"] {
            self.createTargetWith(texture: uwShiethLayer)
        }
        
        if let uwExternalLayer = cableDictionary["externalLayer"] {
            self.createTargetWith(texture: uwExternalLayer)
        }
    }
    
    func fromMaterialToShort(material: String) -> String {
//TODO: fullfill names
        switch material {
        case "Медь":
            return "В"
        case "Алюминий":
            return "А"
        case "PVC":
            return "В"
        default:
            break
        }
        
        return material
    }
    
    
    func initView(){
        
        gameView.allowsCameraControl = true
        gameView.autoenablesDefaultLighting = true
        gameView.backgroundColor = UIColor.init(red: 244/255, green: 245/255, blue: 223/255, alpha: 1.0)
    }
    
    func initScene (){
        gameScene = SCNScene()
        gameView.scene = gameScene
        
        gameView.isPlaying = true
    }
    
    func initCamera() {
        
        currentHeight = 8.0
        currentRadius = 0.3
        cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        
        cameraNode.position = SCNVector3(x: 0, y:0, z: 14)
        
        gameScene.rootNode.addChildNode(cameraNode)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    @IBAction func showAddingNewLayerButtonPressed(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditCableViewController") as? EditCableViewController {
            vc.title = "Конструктор"
            vc.cableDictionary = self.cableDictionary
            vc.materials = self.materials
            vc.features = self.features
            vc.voltage = self.voltage
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func sendEmailButtonPressed(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SendEmailViewController") as? SendEmailViewController {
            vc.title = "Письмо"
            vc.cableDictionary = self.cableDictionary
            vc.materials = self.materials
            vc.features = self.features
            vc.voltage = self.voltage
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func showMainMenuButtonPressed(_ sender: Any) {
        showMenu(button: sender as! UIButton)
    }
    
    func showMenu(button: UIButton!){
        
        globalBackgroundView = UIView()
        mainView = UIView()
        aboutUsView = UIView()
        instructionView = UIView()
        
        initialMainView()
        
        self.view.addSubview(globalBackgroundView!)
        self.view.addSubview(instructionView!)
        self.view.addSubview(aboutUsView!)
        self.view.addSubview(mainView!)
        
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            self.eventualView()
        }, completion: { finished in
            self.addMenuButtons()
        })
    }
    
    func showMenuFromAboutUs(button: UIButton!){
        for view in (self.aboutUsView?.subviews)! {
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            self.eventualView()
        }, completion: { finished in
            self.addMenuButtons()
        })
    }
    
    func showMenuFromInstructions(button: UIButton!){
        print("show menu from instructions")
        
        globalBackgroundView = UIView()
        mainView = UIView()
        aboutUsView = UIView()
        instructionView?.removeFromSuperview()
        instructionView = UIView()
        
        self.view.addSubview(globalBackgroundView!)
        self.view.addSubview(mainView!)
        self.view.addSubview(aboutUsView!)
        self.view.addSubview(instructionView!)
        
        self.initialInstructionsView()
        
        for view in (self.instructionView?.subviews)! {
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            self.eventualView()
        }, completion: { finished in
            self.addMenuButtons()
        })
    }
    
    func showConstructorView(button: UIButton) {
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            for view in (self.mainView?.subviews)! {
                view.removeFromSuperview()
            }
            self.initialMainView()
        }, completion: { finished in
            
            self.globalBackgroundView?.removeFromSuperview()
            self.mainView?.removeFromSuperview()
            self.instructionView?.removeFromSuperview()
            self.aboutUsView?.removeFromSuperview()
        })
    }
    
    func showAboutUsView(button: UIButton!) {
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            for view in (self.aboutUsView?.subviews)! {
                view.removeFromSuperview()
            }
            self.initialAboutUsView()
            
        }, completion: { finished in
            let showMenuButton = UIButton()
            showMenuButton.setTitle("Меню", for: .normal)
            showMenuButton.setTitleColor(UIColor.init(red: 119/255, green: 182/255, blue: 234/255, alpha: 1.0), for: .normal)
            showMenuButton.frame = CGRect.init(x: 16, y: 16, width: 60, height: 44)
            showMenuButton.addTarget(self, action: #selector(self.showMenuFromAboutUs(button:)), for: .touchUpInside)
            showMenuButton.contentHorizontalAlignment = .left
            self.aboutUsView?.addSubview(showMenuButton)
            
            let contentLabel = UILabel()
            contentLabel.numberOfLines = 0
            contentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            contentLabel.textColor = UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0)
            contentLabel.text = "О нас\n\nОсновным направлением деятельности является поставка оборудования и материалов для строительства, капитального ремонта и эксплуатации объектов.\n            Многолетние устойчивые отношения с заводами производителями, профессиональный и заботливый коллектив, работающая транспортная база со складами, необходимым товарным запасом, погрузчиками и автопарком, – все это делает обслуживание наших клиентов быстрым и удобным."
            contentLabel.frame = CGRect.init(x: 16, y: 80, width: self.view.frame.size.width - 32, height: 200)
            
            self.aboutUsView?.addSubview(contentLabel)
//            self.aboutUsView?.removeFromSuperview()
        })
    }
    
    func showInstructionsView(button: UIButton!) {
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            for view in (self.instructionView?.subviews)! {
                view.removeFromSuperview()
            }
            self.initialInstructionsView()
            
        }, completion: { finished in
            
            self.aboutUsView?.removeFromSuperview()
            self.mainView?.removeFromSuperview()
            self.globalBackgroundView?.removeFromSuperview()
            
            let showMenuButtonFromInstruction = UIButton()
            showMenuButtonFromInstruction.setTitle("Меню", for: .normal)
            showMenuButtonFromInstruction.frame = CGRect.init(x: 16, y: 16, width: 60, height: 44)
            showMenuButtonFromInstruction.addTarget(self, action: #selector(self.showMenuFromInstructions(button:)), for: .touchUpInside)
            
            showMenuButtonFromInstruction.contentHorizontalAlignment = .left
            showMenuButtonFromInstruction.setTitleColor(UIColor.init(red: 119/255, green: 182/255, blue: 234/255, alpha: 1.0), for: .normal)
            
            self.instructionView?.addSubview(showMenuButtonFromInstruction)
            
            let contentLabel = UILabel()
            contentLabel.numberOfLines = 0
            contentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            contentLabel.textColor = UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0)
            contentLabel.text = "Инструкция\n\n Используйте данное приложение для создания кабеля. \n\n Шаг 1: Выберите пункт \"Конструктор\" в главном меню."
            contentLabel.frame = CGRect.init(x: 16, y: 40, width: self.view.frame.size.width - 32, height: 200)
            
            self.instructionView?.addSubview(contentLabel)
            
        })
    }
    
    func eventualView() {
        self.globalBackgroundView!.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.50)
        
        self.mainView!.frame = CGRect.init(x: 0, y: self.view.frame.size.height / 3, width: self.view.frame.size.width, height: self.view.frame.size.height / 3)
        
        self.aboutUsView!.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height / 3)
    
        self.aboutUsView!.backgroundColor = UIColor.init(red: 119/255, green: 182/255, blue: 234/255, alpha: 1.0)
        
        self.instructionView!.frame = CGRect.init(x: 0, y: self.view.frame.height * (2 / 3), width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.instructionView!.backgroundColor = UIColor.init(red: 68/255, green: 64/255, blue: 84/255, alpha: 1.0)
        
        self.addLayerButton.isHidden = true
        self.sendEmailButton.isHidden = true
    }
    
    func initialMainView() {
        self.globalBackgroundView!.frame = self.view.frame
        self.globalBackgroundView!.backgroundColor = UIColor.clear
        
        self.mainView!.frame = CGRect.init(x: 0,
                                           y: 0,
                                           width: self.view.frame.size.width,
                                           height: self.view.frame.size.height)
        self.mainView!.backgroundColor = UIColor.clear
        
        self.aboutUsView!.frame = CGRect.init(x: 0,
                                              y: 0 - (self.view.frame.size.height / 3),
                                              width: self.view.frame.size.width,
                                              height: self.view.frame.size.height / 3)
//        self.aboutUsView!.backgroundColor = UIColor.clear
        
        self.instructionView!.frame = CGRect.init(x: 0,
                                                  y: self.view.frame.height * 1.33,
                                                  width: self.view.frame.size.width,
                                                  height: self.view.frame.size.height)
//        self.instructionView!.backgroundColor = UIColor.clear
        
        self.addLayerButton.isHidden = false
        self.sendEmailButton.isHidden = false
    }
    
    func initialAboutUsView() {
        self.mainView!.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        self.aboutUsView!.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.aboutUsView!.backgroundColor = UIColor.init(red: 244/255, green: 245/255, blue: 223/255, alpha: 1.0)

        self.instructionView!.frame = CGRect.init(x: 0, y: self.view.frame.height * 1.66, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    func initialInstructionsView() {
        self.globalBackgroundView!.frame = self.view.frame
        self.instructionView?.layer.zPosition = 9
        
        self.mainView!.frame = CGRect.init(x: 0, y: (-1 / 3) * self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
//        self.mainView!.backgroundColor = UIColor.clear
        
        self.aboutUsView!.frame = CGRect.init(x: 0, y: 0 - (self.view.frame.size.height * 2 / 3), width: self.view.frame.size.width, height: self.view.frame.size.height / 3)
        
        self.instructionView!.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.instructionView!.backgroundColor = UIColor.init(red: 244/255, green: 245/255, blue: 223/255, alpha: 1.0)
    }
    
    func addMenuButtons() {
        let mainViewButton = UIButton()
        mainViewButton.setTitle("Конструктор", for: .normal)
        mainViewButton.frame = CGRect.init(x: 0, y: 0, width: self.mainView!.frame.size.width, height: self.mainView!.frame.size.height)
        mainViewButton.addTarget(self, action: #selector(self.showConstructorView(button:)), for: .touchUpInside)
        
        self.mainView?.addSubview(mainViewButton)
        
        let aboutUsViewButton = UIButton()
        aboutUsViewButton.setTitle("О нас", for: .normal)
        aboutUsViewButton.frame = CGRect.init(x: 0, y: 0, width: self.mainView!.frame.size.width, height: self.mainView!.frame.size.height)
        aboutUsViewButton.addTarget(self, action: #selector(self.showAboutUsView(button:)), for: .touchUpInside)
        
        self.aboutUsView?.addSubview(aboutUsViewButton)
        
        let instructionsButton = UIButton()
        instructionsButton.setTitle("Инструкция", for: .normal)
        instructionsButton.frame = CGRect.init(x: 0, y: 0, width: self.mainView!.frame.size.width, height: self.mainView!.frame.size.height)
        instructionsButton.addTarget(self, action: #selector(self.showInstructionsView(button:)), for: .touchUpInside)
        
        self.instructionView?.addSubview(instructionsButton)
    }
    
    func createTargetWith(texture: UIImage) {
        
        // limitation on number of cylinders
        if gameScene.rootNode.childNodes.count > 13 {
            UserInterface.shared.showWarningWith(message: "Количество слоев ограничено!", on: self)
            
            return
        }
        
        // decreasing a height and increasing a radius for a new cylinder
        self.currentHeight -= 0.5
        self.currentRadius += 0.2
        
        let geometry:SCNGeometry = SCNCylinder.init(radius: currentRadius, height: currentHeight)
        
        geometry.materials.first?.diffuse.contents = texture
        
        let geometryNode = SCNNode(geometry: geometry)
        
        if (gameScene.rootNode.childNodes.count == 0){
            geometryNode.name = "Conductor"
        }
        
        gameScene.rootNode.addChildNode(geometryNode)
    }
    
}

extension GameViewController: DataEnteredDelegate {
    func userDidEnterInformation(info: UIImage, layer: String) {
        self.cableDictionary[layer] = info
    }
    func addMaterialToArray(material: String, layer: String) {
        self.materials[layer] = material
    }
    func addFeaturesToArray(features: [String]) {
        self.features = features
    }
    func updateVoltage(voltage: String) {
        self.voltage = voltage
    }
}


// hiding keyboard
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
