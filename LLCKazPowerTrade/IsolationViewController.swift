
//
//  IsolationViewController.swift
//  LLCKazPowerTrade
//
//  Created by mirasarim on 4/19/17.
//  Copyright © 2017 mirasarim. All rights reserved.
//

import UIKit
import CoreActionSheetPicker

class IsolationViewController: UIViewController {
    
    @IBOutlet weak var LSOHSwitch: UISwitch!
    @IBOutlet weak var FRSwitch: UISwitch!
    @IBOutlet weak var UltravioletSwitch: UISwitch!
    @IBOutlet weak var selectMaterialButton: UIButton!
    
    weak var delegate: DataEnteredDelegate?
    weak var pagingDelegate: PagingMenuDelegate?
    var selectedMaterial = #imageLiteral(resourceName: "pvc")
    var layer = ""
    var selectedMaterialString = ""
    var selectedFeatures:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        
        self.FRSwitch.isOn = false
        self.LSOHSwitch.isOn = false
        self.UltravioletSwitch.isOn = false
        for feature in selectedFeatures {
            if feature == "LSOH" {
                LSOHSwitch.isOn = true
            } else if feature == "FR" {
                FRSwitch.isOn = true
            } else if feature == "UV" {
                UltravioletSwitch.isOn = true
            }
            
        }
    }
    
    @IBAction func selectMaterialButtonPressed(_ sender: Any) {
        showACP(sender)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        if LSOHSwitch.isOn {
            self.selectedFeatures.append("LSOH")
        }
        if FRSwitch.isOn {
            self.selectedFeatures.append("FR")
        }
        if UltravioletSwitch.isOn {
            self.selectedFeatures.append("UV")
        }
        
        delegate?.userDidEnterInformation(info: selectedMaterial, layer: self.layer)
        delegate?.addMaterialToArray(material: selectedMaterialString, layer: self.layer)
        delegate?.addFeaturesToArray(features: selectedFeatures)
        
        pagingDelegate?.moveTo(pageIndex: 2)
    }
    
    
    func showACP(_ sender: Any) {
        let bonusesToACP = ["ПВХ", "Резина", "Сшитый полиэтилен", "Бумага пропитаная маслом"]
        
        if let acp = ActionSheetStringPicker(title: "Материалы:", rows: bonusesToACP, initialSelection: 0, doneBlock: {
            picker, indexes, values in
//            print("\(picker) \n--- \(indexes) \n--- \(values)")
            self.selectedMaterialString = values as! String
            if indexes == 0 {
                self.selectedMaterial = #imageLiteral(resourceName: "pvc")
                self.selectMaterialButton.setTitle("Выберите материал. (PVC)", for: .normal)
            } else if indexes == 1 {
                self.selectedMaterial = #imageLiteral(resourceName: "rezina")
                self.selectMaterialButton.setTitle("Выберите материал. (Резина)", for: .normal)
            } else if indexes == 2 {
                self.selectedMaterial = #imageLiteral(resourceName: "xlpe")
                self.selectMaterialButton.setTitle("Выберите материал. (XLPE)", for: .normal)
            } else if indexes == 3 {
                self.selectedMaterial = #imageLiteral(resourceName: "kraftPaper")
                self.selectMaterialButton.setTitle("Выберите материал. (Бумага)", for: .normal)
            }
            
            return
        }, cancel: { ActionSheetStringPicker in return }, origin: sender) {
            acp.setDoneButton(UIBarButtonItem(title: "Готово", style: .plain, target: nil, action: nil))
            acp.setCancelButton(UIBarButtonItem(title: "Отмена", style: .plain, target: nil, action: nil))
            acp.show()
        }
    }
}
